﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MinutesMatter.Api.Migrations.SqlServerMigrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    Role = table.Column<string>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: true),
                    ChangePassword = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Agency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ServiceRadius = table.Column<int>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: false),
                    SubValue = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AgencyAccout",
                columns: table => new
                {
                    AgencyId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgencyAccout", x => new { x.AgencyId, x.AccountId });
                    table.ForeignKey(
                        name: "FK_AgencyAccout_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgencyAccout_Agency_AgencyId",
                        column: x => x.AgencyId,
                        principalTable: "Agency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AgencyId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Agency_AgencyId",
                        column: x => x.AgencyId,
                        principalTable: "Agency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: false),
                    NamePrefix = table.Column<string>(nullable: true),
                    NameSuffix = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone1 = table.Column<string>(nullable: true),
                    Phone1SMSEnabled = table.Column<bool>(nullable: true),
                    Phone2 = table.Column<string>(nullable: true),
                    Phone2SMSEnabled = table.Column<bool>(nullable: true),
                    ContactTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contact_Category_ContactTypeId",
                        column: x => x.ContactTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomerAccount",
                columns: table => new
                {
                    CustomerId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerAccount", x => new { x.CustomerId, x.AccountId });
                    table.ForeignKey(
                        name: "FK_CustomerAccount_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CustomerAccount_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Member",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(nullable: false),
                    PictureLink = table.Column<string>(nullable: true),
                    Pin = table.Column<string>(maxLength: 6, nullable: true),
                    CitizenshipId = table.Column<int>(nullable: true),
                    CustomerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Member", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Member_Category_CitizenshipId",
                        column: x => x.CitizenshipId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Member_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserAccount",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccount", x => new { x.UserId, x.AccountId });
                    table.ForeignKey(
                        name: "FK_UserAccount_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserAccount_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountContact",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(nullable: false),
                    ContactId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountContact", x => new { x.ContactId, x.AccountId });
                    table.ForeignKey(
                        name: "FK_AccountContact_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountContact_Contact_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contact",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MemberAccount",
                columns: table => new
                {
                    MemberId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberAccount", x => new { x.MemberId, x.AccountId });
                    table.ForeignKey(
                        name: "FK_MemberAccount_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MemberAccount_Member_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MemberId = table.Column<Guid>(nullable: false),
                    ReceiveTextMessages = table.Column<bool>(nullable: false),
                    PincodeRequiredForPHI = table.Column<bool>(nullable: false),
                    WaiverSigned = table.Column<bool>(nullable: false),
                    WaiverSignedDate = table.Column<DateTime>(nullable: true),
                    ConfidentialityAgreementSigned = table.Column<bool>(nullable: false),
                    ConfidentialityAgreementSignedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permissions_Member_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonalHealthInformation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MemberId = table.Column<Guid>(nullable: false),
                    BloodTypeId = table.Column<int>(nullable: true),
                    HospitalPreference = table.Column<string>(nullable: true),
                    InsuranceCompany = table.Column<string>(nullable: true),
                    InsuranceNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalHealthInformation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonalHealthInformation_Category_BloodTypeId",
                        column: x => x.BloodTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalHealthInformation_Member_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Allergy",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    AllergyTypeId = table.Column<int>(nullable: true),
                    SeverityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allergy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Allergy_Category_AllergyTypeId",
                        column: x => x.AllergyTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Allergy_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Allergy_Category_SeverityId",
                        column: x => x.SeverityId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentEntry",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Link = table.Column<string>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    DocumentTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentEntry", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentEntry_Category_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentEntry_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExistingCondition",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DiagnosedDate = table.Column<DateTime>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    SeverityId = table.Column<int>(nullable: true),
                    ExistingConditionTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExistingCondition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExistingCondition_Category_ExistingConditionTypeId",
                        column: x => x.ExistingConditionTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExistingCondition_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExistingCondition_Category_SeverityId",
                        column: x => x.SeverityId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Injury",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DateOccurred = table.Column<DateTime>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    InjuryTypeId = table.Column<int>(nullable: true),
                    SeverityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Injury", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Injury_Category_InjuryTypeId",
                        column: x => x.InjuryTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Injury_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Injury_Category_SeverityId",
                        column: x => x.SeverityId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Surgery",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DatePerformed = table.Column<DateTime>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    SurgeryTypeId = table.Column<int>(nullable: true),
                    SeverityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surgery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Surgery_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Surgery_Category_SeverityId",
                        column: x => x.SeverityId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Surgery_Category_SurgeryTypeId",
                        column: x => x.SurgeryTypeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Medication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PersonalHealthInformationId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    MedicationPurposeId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DatePrescribed = table.Column<DateTime>(nullable: true),
                    Dosage = table.Column<string>(nullable: true),
                    MedicationUsageId = table.Column<int>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    AssociatedExistingConditionId = table.Column<Guid>(nullable: true),
                    AssociatedAllergyId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Medication_Allergy_AssociatedAllergyId",
                        column: x => x.AssociatedAllergyId,
                        principalTable: "Allergy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medication_ExistingCondition_AssociatedExistingConditionId",
                        column: x => x.AssociatedExistingConditionId,
                        principalTable: "ExistingCondition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medication_Category_MedicationPurposeId",
                        column: x => x.MedicationPurposeId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medication_Category_MedicationUsageId",
                        column: x => x.MedicationUsageId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medication_PersonalHealthInformation_PersonalHealthInformationId",
                        column: x => x.PersonalHealthInformationId,
                        principalTable: "PersonalHealthInformation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountContact_AccountId",
                table: "AccountContact",
                column: "AccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccountContact_ContactId",
                table: "AccountContact",
                column: "ContactId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AgencyAccout_AccountId",
                table: "AgencyAccout",
                column: "AccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Allergy_AllergyTypeId",
                table: "Allergy",
                column: "AllergyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Allergy_PersonalHealthInformationId",
                table: "Allergy",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Allergy_SeverityId",
                table: "Allergy",
                column: "SeverityId");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_ContactTypeId",
                table: "Contact",
                column: "ContactTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAccount_AccountId",
                table: "CustomerAccount",
                column: "AccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentEntry_DocumentTypeId",
                table: "DocumentEntry",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentEntry_PersonalHealthInformationId",
                table: "DocumentEntry",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ExistingCondition_ExistingConditionTypeId",
                table: "ExistingCondition",
                column: "ExistingConditionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExistingCondition_PersonalHealthInformationId",
                table: "ExistingCondition",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_ExistingCondition_SeverityId",
                table: "ExistingCondition",
                column: "SeverityId");

            migrationBuilder.CreateIndex(
                name: "IX_Injury_InjuryTypeId",
                table: "Injury",
                column: "InjuryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Injury_PersonalHealthInformationId",
                table: "Injury",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Injury_SeverityId",
                table: "Injury",
                column: "SeverityId");

            migrationBuilder.CreateIndex(
                name: "IX_Medication_AssociatedAllergyId",
                table: "Medication",
                column: "AssociatedAllergyId");

            migrationBuilder.CreateIndex(
                name: "IX_Medication_AssociatedExistingConditionId",
                table: "Medication",
                column: "AssociatedExistingConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_Medication_MedicationPurposeId",
                table: "Medication",
                column: "MedicationPurposeId");

            migrationBuilder.CreateIndex(
                name: "IX_Medication_MedicationUsageId",
                table: "Medication",
                column: "MedicationUsageId");

            migrationBuilder.CreateIndex(
                name: "IX_Medication_PersonalHealthInformationId",
                table: "Medication",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Member_CitizenshipId",
                table: "Member",
                column: "CitizenshipId");

            migrationBuilder.CreateIndex(
                name: "IX_Member_CustomerId",
                table: "Member",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberAccount_AccountId",
                table: "MemberAccount",
                column: "AccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MemberAccount_MemberId",
                table: "MemberAccount",
                column: "MemberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_MemberId",
                table: "Permissions",
                column: "MemberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonalHealthInformation_BloodTypeId",
                table: "PersonalHealthInformation",
                column: "BloodTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalHealthInformation_MemberId",
                table: "PersonalHealthInformation",
                column: "MemberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Surgery_PersonalHealthInformationId",
                table: "Surgery",
                column: "PersonalHealthInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Surgery_SeverityId",
                table: "Surgery",
                column: "SeverityId");

            migrationBuilder.CreateIndex(
                name: "IX_Surgery_SurgeryTypeId",
                table: "Surgery",
                column: "SurgeryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_User_AgencyId",
                table: "User",
                column: "AgencyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccount_AccountId",
                table: "UserAccount",
                column: "AccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserAccount_UserId",
                table: "UserAccount",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountContact");

            migrationBuilder.DropTable(
                name: "AgencyAccout");

            migrationBuilder.DropTable(
                name: "CustomerAccount");

            migrationBuilder.DropTable(
                name: "DocumentEntry");

            migrationBuilder.DropTable(
                name: "Injury");

            migrationBuilder.DropTable(
                name: "Medication");

            migrationBuilder.DropTable(
                name: "MemberAccount");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "Surgery");

            migrationBuilder.DropTable(
                name: "UserAccount");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropTable(
                name: "Allergy");

            migrationBuilder.DropTable(
                name: "ExistingCondition");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "PersonalHealthInformation");

            migrationBuilder.DropTable(
                name: "Agency");

            migrationBuilder.DropTable(
                name: "Member");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Customer");
        }
    }
}
