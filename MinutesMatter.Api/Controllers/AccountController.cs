﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MinutesMatter.Api.Entities;
using MinutesMatter.Api.Helpers;
using MinutesMatter.Api.Models.Accounts;
using MinutesMatter.Api.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;
        private readonly IMapper mapper;
        private readonly AppSettings appSettings;

        public AccountController(
            IAccountService accountService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            this.accountService = accountService;
            this.mapper = mapper;
            this.appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateModel model)
        {
            try
            {
                var account = await accountService.Authenticate(model.Username, model.Password);

                if (account == null)
                    return BadRequest(new { message = "Username or password is incorrect" });
                if (!account.Enabled)
                    return BadRequest(new { message = "Account is disabled" });

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(appSettings.Secret);
                var tokenDescrpitor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, account.Id.ToString()),
                        new Claim(ClaimTypes.Role, account.Role)
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescrpitor);
                var tokenString = tokenHandler.WriteToken(token);

                // return basic account info and authentication token
                return Ok(new
                {
                    account.Id,
                    account.Username,
                    account.Role,
                    Token = tokenString
                });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            // map model to entity
            var account = mapper.Map<Account>(model);

            // only allow an admin to specify the users' role, default to Member role if Admin is not creating a account
            if (!User.IsInRole(Role.Admin))
                account.Role = Role.Member;

            if (User.IsInRole(Role.Agency) && account.Role != Role.User) // An 'Agency' can only create users of role 'User'
                return BadRequest(new { message = "An Agency account can only create Users" });
            else if (User.IsInRole(Role.Customer) && account.Role != Role.Member) // A 'Customer' can only create users of role 'Member'
                return BadRequest(new { message = "A Customer account can only create Members" });
            else if (!User.IsInRole(Role.Admin)) // An Admin can create any kind of account, if not Admin, set Role to Member by default
                account.Role = Role.Member;

            try
            {
                // create account
                await accountService.Create(account, model.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var accounts = await accountService.GetAll();
                var model = mapper.Map<IList<AccountModel>>(accounts);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            try
            {
                // only allow admins to access other account records
                var currentUserId = User.Identity.Name;
                if (id != currentUserId && !User.IsInRole(Role.Admin))
                    return Forbid();

                var account = await accountService.GetById(new Guid(id));

                if (account == null)
                    return NotFound();

                var model = mapper.Map<AccountModel>(account);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPut("{id}/enable")]
        public async Task<IActionResult> Enable(string id)
        {
            try
            {
                // get the account
                var account = await accountService.GetById(new Guid(id));

                if (account == null)
                    return NotFound();

                // enable the account
                account.Enabled = true;

                // update the account
                if (await accountService.Update(account))
                    return Ok();
                else
                    return BadRequest(new { message = "There was an issue enabling the account." });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPut("{id}/disable")]
        public async Task<IActionResult> Disable(string id)
        {
            try
            {
                // get the account
                var account = await accountService.GetById(new Guid(id));

                if (account == null)
                    return NotFound();

                // disable the account
                account.Enabled = false;

                // update the account
                if (await accountService.Update(account))
                    return Ok();
                else
                    return BadRequest(new { message = "There was an issue disabling the account." });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(string id, [FromBody]UpdateModel model)
        {
            // only allow admins to access other account records
            var currentUserId = User.Identity.Name;
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();

            // map model to entity and set id
            var account = mapper.Map<Account>(model);
            account.Id = new Guid(id);

            try
            {
                // update account
                if (await accountService.Update(account, model.Password))
                    return Ok();
                else
                    return BadRequest(new { message = "There was an issue updating the account" });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                // only allow admins to access other account records
                var currentUserId = User.Identity.Name;
                if (id != currentUserId && !User.IsInRole(Role.Admin))
                    return Forbid();

                if (await accountService.Delete(new Guid(id)))
                    return Ok();
                else
                    return BadRequest(new { message = "There was an issue deleting the account" });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}