﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class Permissions
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey(nameof(Member))]
        public Guid MemberId { get; set; }
        public Member Member { get; set; }
        [Required]
        public bool ReceiveTextMessages { get; set; }
        [Required]
        public bool PincodeRequiredForPHI { get; set; }
        [Required]
        public bool WaiverSigned { get; set; }
        public DateTime? WaiverSignedDate { get; set; }
        [Required]
        public bool ConfidentialityAgreementSigned { get; set; }
        public DateTime? ConfidentialityAgreementSignedDate { get; set; }
    }
}
