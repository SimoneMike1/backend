﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class AccountContact
    {
        [Key, ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public Account Account { get; set; }
        [Key, ForeignKey(nameof(Contact))]
        public Guid ContactId { get; set; }
        public Contact Contact { get; set; }
    }
}
