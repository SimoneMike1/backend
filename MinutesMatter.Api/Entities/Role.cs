﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public static class Role
    {
        public const string Admin = "Admin";
        public const string User = "User";
        public const string Customer = "Customer";
        public const string Member = "Member";
        public const string Agency = "Agency";
    }
}
