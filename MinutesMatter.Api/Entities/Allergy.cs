﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class Allergy
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [ForeignKey(nameof(PersonalHealthInformation))]
        public Guid PersonalHealthInformationId { get; set; }
        public PersonalHealthInformation PersonalHealthInformation { get; set; }
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public Category AllergyType { get; set; }
        public Category Severity { get; set; }
    }
}
