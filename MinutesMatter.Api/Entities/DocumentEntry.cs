﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class DocumentEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [ForeignKey(nameof(PersonalHealthInformation))]
        public Guid PersonalHealthInformationId { get; set; }
        public PersonalHealthInformation PersonalHealthInformation { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Link { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Category DocumentType { get; set; }
    }
}
