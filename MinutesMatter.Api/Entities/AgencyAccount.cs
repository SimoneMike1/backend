﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class AgencyAccount
    {
        [Key, ForeignKey(nameof(Agency))]
        public Guid AgencyId { get; set; }
        public Agency Agency { get; set; }
        [Key, ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public Account Account { get; set; }
    }
}
