﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class PersonalHealthInformation
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey(nameof(Member))]
        public Guid MemberId { get; set; }
        public Member Member { get; set; }
        public Category BloodType { get; set; }
        public string HospitalPreference { get; set; }
        public string InsuranceCompany { get; set; }
        public string InsuranceNumber { get; set; }
        
        public ICollection<Allergy> Allergies { get; set; }
        public ICollection<DocumentEntry> DocumentEntries { get; set; }
        public ICollection<ExistingCondition> ExistingConditions { get; set; }
        public ICollection<Injury> Injuries { get; set; }
        public ICollection<Medication> Medication { get; set; }
        public ICollection<Surgery> Surgeries { get; set; }
    }
}
