﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Entities
{
    public class Member
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Gender { get; set; }
        public string PictureLink { get; set; }
        [MaxLength(6)]
        public string Pin { get; set; }
        public Category Citizenship { get; set; }
        public Customer Customer { get; set; }
        public Permissions Permissions { get; set; }
        public PersonalHealthInformation PersonalHealthInformation { get; set; }
    }
}
