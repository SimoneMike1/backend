﻿using MinutesMatter.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Services.Interfaces
{
    public interface IAccountService
    {
        Task<Account> Authenticate(string username, string password);
        Task<IEnumerable<Account>> GetAll();
        Task<Account> GetByUsername(string username);
        Task<Account> GetById(Guid id);
        Task<Account> Create(Account account, string password);
        Task<bool> Update(Account account, string password = null);
        Task<bool> Delete(Guid id);
    }
}
