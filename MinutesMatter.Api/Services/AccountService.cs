﻿using Microsoft.EntityFrameworkCore;
using MinutesMatter.Api.Entities;
using MinutesMatter.Api.Helpers;
using MinutesMatter.Api.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Services
{
    public class AccountService : IAccountService
    {
        private readonly DataContext context;

        public AccountService(DataContext context)
        {
            this.context = context;
        }

        public async Task<Account> Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var account = await context.Account.SingleOrDefaultAsync(x => x.Username == username);

            // check if username exists
            if (account == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, account.PasswordHash, account.PasswordSalt))
                return null;

            // authentication successful
            return account;
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            return await context.Account.ToListAsync();
        }

        public async Task<Account> GetById(Guid id)
        {
            return await context.Account.FindAsync(id);
        }

        public async Task<Account> Create(Account account, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (await context.Account.AnyAsync(x => x.Username == account.Username))
                throw new AppException("Username \"" + account.Username + "\" is already taken");

            // enable the account
            account.Enabled = true;

            account.DateCreated = DateTime.UtcNow;

            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            account.PasswordHash = passwordHash;
            account.PasswordSalt = passwordSalt;

            context.Account.Add(account);
            await context.SaveChangesAsync();

            return account;
        }

        public async Task<bool> Update(Account accountParam, string password = null)
        {
            var account = await context.Account.FindAsync(accountParam.Id);

            if (account == null)
                throw new AppException("User not found");

            // update username if it has changes
            if(!string.IsNullOrWhiteSpace(accountParam.Username) && accountParam.Username != account.Username)
            {
                // throw error if the new username is already taken
                if (await context.Account.AnyAsync(x => x.Username == accountParam.Username))
                    throw new AppException("Username " + accountParam.Username + " is already taken");

                account.Username = accountParam.Username;
            }

            // update account properties if provided
            if (!string.IsNullOrWhiteSpace(accountParam.Role))
                account.Role = accountParam.Role;

            if (account.Enabled != accountParam.Enabled)
                account.Enabled = accountParam.Enabled;

            if (account.ChangePassword != accountParam.ChangePassword)
                account.ChangePassword = accountParam.ChangePassword;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                account.PasswordHash = passwordHash;
                account.PasswordSalt = passwordSalt;
            }

            account.LastModified = DateTime.UtcNow;

            context.Account.Update(account);
            return (await context.SaveChangesAsync() > 0);
        }

        public async Task<bool> Delete(Guid id)
        {
            var account = await context.Account.FindAsync(id);

            if (account == null)
                throw new AppException("Account not found");

            context.Account.Remove(account);
            return (await context.SaveChangesAsync() > 0);
        }

        public async Task<Account> GetByUsername(string username)
        {
            return await context.Account.Where(x => x.Username == username).FirstOrDefaultAsync();
        }

        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException("Value cannot be empty or whitespace only string.", "password");

            using var hmac = new HMACSHA512();
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64)
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128)
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordSalt");

            using(var hmac = new HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i])
                        return false;
                }
            }

            return true;
        }
    }
}
