﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MinutesMatter.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Helpers
{
    public class DataContext : DbContext
    {
        public DbSet<Account> Account { get; set; }
        public DbSet<AccountContact> AccountContact { get; set; }
        public DbSet<Agency> Agency { get; set; }
        public DbSet<AgencyAccount> AgencyAccout { get; set; }
        public DbSet<Allergy> Allergy { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerAccount> CustomerAccount { get; set; }
        public DbSet<DocumentEntry> DocumentEntry { get; set; }
        public DbSet<ExistingCondition> ExistingCondition { get; set; }
        public DbSet<Injury> Injury { get; set; }
        public DbSet<Medication> Medication { get; set; }
        public DbSet<Member> Member { get; set; }
        public DbSet<MemberAccount> MemberAccount { get; set; }
        public DbSet<Permissions> Permissions { get; set; }
        public DbSet<PersonalHealthInformation> PersonalHealthInformation { get; set; }
        public DbSet<Surgery> Surgery { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserAccount> UserAccount { get; set; }

        protected readonly IConfiguration config;

        public DataContext(DbContextOptions<DataContext> options, IConfiguration config)
            : base(options)
        {
            this.config = config;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // connect to sql server database
            optionsBuilder.UseSqlServer(config.GetConnectionString("MinutesMatterDb"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetUpRelationships(modelBuilder);

            SeedData(modelBuilder);
        }

        protected void SetUpRelationships(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemberAccount>().HasKey(ma => new { ma.MemberId, ma.AccountId });
            modelBuilder.Entity<MemberAccount>()
                .HasOne<Member>(ma => ma.Member)
                .WithOne();
            modelBuilder.Entity<MemberAccount>()
                .HasOne<Account>(ma => ma.Account)
                .WithOne();

            modelBuilder.Entity<UserAccount>().HasKey(ua => new { ua.UserId, ua.AccountId });
            modelBuilder.Entity<UserAccount>()
                .HasOne<User>(ua => ua.User)
                .WithOne();
            modelBuilder.Entity<UserAccount>()
                .HasOne<Account>(ua => ua.Account)
                .WithOne();

            modelBuilder.Entity<AccountContact>().HasKey(ac => new { ac.ContactId, ac.AccountId });
            modelBuilder.Entity<AccountContact>()
                .HasOne<Contact>(ac => ac.Contact)
                .WithOne();
            modelBuilder.Entity<AccountContact>()
                .HasOne<Account>(ac => ac.Account)
                .WithOne();

            modelBuilder.Entity<CustomerAccount>().HasKey(ca => new { ca.CustomerId, ca.AccountId });
            modelBuilder.Entity<CustomerAccount>()
                .HasOne<Customer>(ca => ca.Customer)
                .WithMany();
            modelBuilder.Entity<CustomerAccount>()
                .HasOne<Account>(ca => ca.Account)
                .WithOne()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<AgencyAccount>().HasKey(aa => new { aa.AgencyId, aa.AccountId });
            modelBuilder.Entity<AgencyAccount>()
                .HasOne<Agency>(aa => aa.Agency)
                .WithMany();
            modelBuilder.Entity<AgencyAccount>()
                .HasOne<Account>(aa => aa.Account)
                .WithOne()
                .OnDelete(DeleteBehavior.Restrict);
        }

        protected void SeedData(ModelBuilder modelBuilder)
        {
            //SeedTypeData(modelBuilder);

            //var customer1 = new Customer { Id = new Guid("FDA1C0ED-78F0-497B-9066-4D9513D0BC0A"), Name = "Florida Gulf Coast University" };
            //modelBuilder.Entity<Customer>().HasData(
            //    customer1
            //    );

            //var agency1 = new Agency { Id = new Guid("9820F2E2-72C4-451B-BA09-04041124193E"), Name = "Health Park", ServiceRadius = 30 };
            //modelBuilder.Entity<Agency>().HasData(
            //    agency1
            //    );

            //var user1 = new { Id = new Guid("8EA265DA-DF69-4874-9833-759191FF2EA1"), AgencyId = agency1.Id };
            //modelBuilder.Entity<User>().HasData(
            //    user1
            //    );

            //CreatePasswordHash("password", out byte[] passwordHash, out byte[] passwordSalt);
            //var adminAccount = new Account { Id = new Guid("BF0EF9A9-1467-4B9E-B210-33FC6EC0EEC9"), Username = "admin", ChangePassword = false, DateCreated = DateTime.UtcNow, Enabled = true, Role = Role.Admin, PasswordHash = passwordHash, PasswordSalt = passwordSalt};
            //var account1 = new Account { Id = new Guid("18C311F5-3286-4804-A4E5-0398F3669ACF"), Username = "dstewart", ChangePassword = false, DateCreated = DateTime.UtcNow, Enabled = true, Role = Role.Member, PasswordHash = passwordHash, PasswordSalt = passwordSalt };
            //var account2 = new Account { Id = new Guid("C3C81A55-26E3-4F07-9472-B3FD981BB936"), Username = "customer", ChangePassword = false, DateCreated = DateTime.UtcNow, Enabled = true, Role = Role.Customer, PasswordHash = passwordHash, PasswordSalt = passwordSalt };
            //var account3 = new Account { Id = new Guid("EA21EA5F-0B78-4119-ACFC-B8959653477F"), Username = "agency", ChangePassword = false, DateCreated = DateTime.UtcNow, Enabled = true, Role = Role.Agency, PasswordHash = passwordHash, PasswordSalt = passwordSalt };
            //var account4 = new Account { Id = new Guid("A66C2301-3F04-4A2A-9CDF-30D3E7BDCAF3"), Username = "user", ChangePassword = false, DateCreated = DateTime.UtcNow, Enabled = true, Role = Role.User, PasswordHash = passwordHash, PasswordSalt = passwordSalt };
            //modelBuilder.Entity<Account>().HasData(
            //    adminAccount,
            //    account1,
            //    account2,
            //    account3,
            //    account4
            //    );

            //var customerAccount1 = new CustomerAccount { CustomerId = customer1.Id, AccountId = account2.Id };
            //modelBuilder.Entity<CustomerAccount>().HasData(
            //    customerAccount1
            //    );

            //var agencyAccount1 = new AgencyAccount { AgencyId = agency1.Id, AccountId = account3.Id };
            //modelBuilder.Entity<AgencyAccount>().HasData(
            //    agencyAccount1
            //    );

            //var userAccount1 = new UserAccount { UserId = user1.Id, AccountId = account4.Id };
            //modelBuilder.Entity<UserAccount>().HasData(
            //    userAccount1
            //    );

            //var contact1 = new { Id = new Guid("5B72186C-AEDB-4305-BA66-D8CD05BCB0F0"), FirstName = "Nivitus", LastName = "Administrator", Email = "admin@nivitus.com", ContactTypeId = 10 };
            //var contact2 = new { Id = new Guid("FAF380D9-6EE5-44AB-894E-DE18981F548C"), FirstName = "Daniel", LastName = "Stewart", Email = "dstew@nivitus.com", ContactTypeId = 10 };
            //var contact3 = new { Id = new Guid("175F4011-F5A7-4715-B543-F8101AB394B1"), FirstName = "John", LastName = "Doe", Email = "jdoe@customer.com", ContactTypeId = 10 };
            //var contact4 = new { Id = new Guid("F11D8DF9-0490-41B0-B33A-9263E426C0A9"), FirstName = "Mike", LastName = "Simone", Email = "mws@nivitus.com", ContactTypeId = 10 };
            //var contact5 = new { Id = new Guid("125A75FC-3055-49B6-9524-F714C5E04919"), FirstName = "Larry", LastName = "Simone", Email = "mls@nivitus.com", ContactTypeId = 10 };
            //modelBuilder.Entity<Contact>().HasData(
            //    contact1,
            //    contact2,
            //    contact3,
            //    contact4,
            //    contact5
            //    );

            //var accountContact1 = new AccountContact { AccountId = adminAccount.Id, ContactId = contact1.Id };
            //var accountContact2 = new AccountContact { AccountId = account1.Id, ContactId = contact2.Id };
            //var accountContact3 = new AccountContact { AccountId = account2.Id, ContactId = contact3.Id };
            //var accountContact4 = new AccountContact { AccountId = account3.Id, ContactId = contact4.Id };
            //var accountContact5 = new AccountContact { AccountId = account4.Id, ContactId = contact5.Id };
            //modelBuilder.Entity<AccountContact>().HasData(
            //    accountContact1,
            //    accountContact2,
            //    accountContact3,
            //    accountContact4,
            //    accountContact5
            //    );

            //var member1 = new { Id = new Guid("18463241-6DCA-435E-9DE5-E5601B9F1378"), DateOfBirth = DateTime.Parse("06 /25/1997"), Gender = "Male", CitizenshipId = 5, CustomerId = customer1.Id };
            //modelBuilder.Entity<Member>().HasData(
            //    member1
            //    );

            //var memberAccount1 = new MemberAccount { MemberId = member1.Id, AccountId = account1.Id };
            //modelBuilder.Entity<MemberAccount>().HasData(
            //    memberAccount1
            //    );

            //var permissions1 = new Permissions { Id = new Guid("A59C34CA-CBF2-421A-B456-40B4CFD33E54"), MemberId = member1.Id, WaiverSigned = true, WaiverSignedDate = DateTime.UtcNow, ConfidentialityAgreementSigned = true, ConfidentialityAgreementSignedDate = DateTime.UtcNow, PincodeRequiredForPHI = false, ReceiveTextMessages = false };
            //modelBuilder.Entity<Permissions>().HasData(
            //    permissions1
            //    );
        }

        protected void SeedTypeData(ModelBuilder modelBuilder)
        {
			var types = new List<Category>
			{
				// ACCESS REASONS
				new Category { Id = 1, Name = "AccessReason", Value = "Trauma" },
				new Category { Id = 2, Name = "AccessReason", Value = "Trauma", SubValue = "MVC" },
				new Category { Id = 3, Name = "AccessReason", Value = "Trauma", SubValue = "Fall" },
				new Category { Id = 4, Name = "AccessReason", Value = "Trauma", SubValue = "Blunt Force" },
				new Category { Id = 5, Name = "AccessReason", Value = "Trauma", SubValue = "Crush" },
				new Category { Id = 6, Name = "AccessReason", Value = "Trauma", SubValue = "Burn" },
				new Category { Id = 7, Name = "AccessReason", Value = "Trauma", SubValue = "Electrocution" },
				new Category { Id = 8, Name = "AccessReason", Value = "Trauma", SubValue = "Drowning" },
				new Category { Id = 9, Name = "AccessReason", Value = "Trauma", SubValue = "Violent" },
				new Category { Id = 10, Name = "AccessReason", Value = "Trauma", SubValue = "Other" },
				new Category { Id = 11, Name = "AccessReason", Value = "Medical" },
				new Category { Id = 12, Name = "AccessReason", Value = "Medical", SubValue = "Cardiac" },
				new Category { Id = 13, Name = "AccessReason", Value = "Medical", SubValue = "Respiratory" },
				new Category { Id = 14, Name = "AccessReason", Value = "Medical", SubValue = "OD" },
				new Category { Id = 15, Name = "AccessReason", Value = "Medical", SubValue = "Pain" },
				new Category { Id = 16, Name = "AccessReason", Value = "Medical", SubValue = "Anaphylaxis" },
				new Category { Id = 17, Name = "AccessReason", Value = "Medical", SubValue = "Pregnancy" },
				new Category { Id = 18, Name = "AccessReason", Value = "Medical", SubValue = "Diabetic" },
				new Category { Id = 19, Name = "AccessReason", Value = "Medical", SubValue = "Malaise" },
				new Category { Id = 20, Name = "AccessReason", Value = "Medical", SubValue = "Other" },
				new Category { Id = 21, Name = "AccessReason", Value = "Follow-Up" },
				new Category { Id = 22, Name = "AccessReason", Value = "Follow-Up", SubValue = "Additional detail" },
				new Category { Id = 23, Name = "AccessReason", Value = "Follow-Up", SubValue = "Correction" },
				new Category { Id = 24, Name = "AccessReason", Value = "Other" },

				// ADDRESS TYPES
				new Category { Id = 25, Name = "Address", Value = "Home" },
				new Category { Id = 26, Name = "Address", Value = "Campus" },
				new Category { Id = 27, Name = "Address", Value = "Off Campus" },
				new Category { Id = 28, Name = "Address", Value = "Office" },
				new Category { Id = 29, Name = "Address", Value = "Business" },
				new Category { Id = 30, Name = "Address", Value = "Main" },
				new Category { Id = 31, Name = "Address", Value = "Billing" },
				new Category { Id = 32, Name = "Address", Value = "Other" },
				new Category { Id = 32, Name = "Address", Value = "Other" },

				// BILLING PERIOD
				new Category { Id = 41, Name = "Billing", Value = "Once" },
				new Category { Id = 42, Name = "Billing", Value = "Yearly" },
				new Category { Id = 43, Name = "Billing", Value = "Bi-annual" },
				new Category { Id = 44, Name = "Billing", Value = "Quarterly" },
				new Category { Id = 45, Name = "Billing", Value = "Monthly" },
				new Category { Id = 46, Name = "Billing", Value = "Other" },

				// BLOOD TYPES
				new Category { Id = 93, Name = "BloodType", Value = "O Negative" },
				new Category { Id = 94, Name = "BloodType", Value = "O Positive" },
				new Category { Id = 95, Name = "BloodType", Value = "A Negative" },
				new Category { Id = 96, Name = "BloodType", Value = "A Positive" },
				new Category { Id = 97, Name = "BloodType", Value = "B Negative" },
				new Category { Id = 98, Name = "BloodType", Value = "B Positive" },
				new Category { Id = 99, Name = "BloodType", Value = "AB Negative" },
				new Category { Id = 100, Name = "BloodType", Value = "AB Positive" },

				// CONTACT TYPES
				new Category { Id = 47, Name = "Contact", Value = "Parent" },
				new Category { Id = 48, Name = "Contact", Value = "Friend" },
				new Category { Id = 49, Name = "Contact", Value = "Sibling" },
				new Category { Id = 50, Name = "Contact", Value = "Family" },
				new Category { Id = 51, Name = "Contact", Value = "Grandparent" },
				new Category { Id = 52, Name = "Contact", Value = "Doctor" },
				new Category { Id = 53, Name = "Contact", Value = "Clergy" },
				new Category { Id = 54, Name = "Contact", Value = "Spouse" },
				new Category { Id = 55, Name = "Contact", Value = "Manager" },
				new Category { Id = 56, Name = "Contact", Value = "Other" },

				// DOCUMENT TYPES
				new Category { Id = 57, Name = "Document", Value = "Waiver" },
				new Category { Id = 58, Name = "Document", Value = "Legal" },
				new Category { Id = 59, Name = "Document", Value = "Medical" },
				new Category { Id = 60, Name = "Document", Value = "Personal" },
				new Category { Id = 61, Name = "Document", Value = "Insurance" },
				new Category { Id = 62, Name = "Document", Value = "Standing Orders" },
				new Category { Id = 63, Name = "Document", Value = "Treatment Plan" },
				new Category { Id = 64, Name = "Document", Value = "Other" },

				// MEDICATION PURPOSE
				new Category { Id = 65, Name = "MedicationPurpose", Value = "Pain" },
				new Category { Id = 66, Name = "MedicationPurpose", Value = "Cardiac" },
				new Category { Id = 67, Name = "MedicationPurpose", Value = "Blood Pressure" },
				new Category { Id = 68, Name = "MedicationPurpose", Value = "Anxiety" },
				new Category { Id = 69, Name = "MedicationPurpose", Value = "Depression" },
				new Category { Id = 70, Name = "MedicationPurpose", Value = "Anticoagulant" },
				new Category { Id = 71, Name = "MedicationPurpose", Value = "Thrombolytics" },
				new Category { Id = 72, Name = "MedicationPurpose", Value = "Seizure" },
				new Category { Id = 73, Name = "MedicationPurpose", Value = "Arthritis" },
				new Category { Id = 74, Name = "MedicationPurpose", Value = "Cancer" },
				new Category { Id = 75, Name = "MedicationPurpose", Value = "Psycological Disorder" },
				new Category { Id = 76, Name = "MedicationPurpose", Value = "Sleeping" },
				new Category { Id = 77, Name = "MedicationPurpose", Value = "Allergy" },
				new Category { Id = 78, Name = "MedicationPurpose", Value = "Asthma" },
				new Category { Id = 79, Name = "MedicationPurpose", Value = "Diabetes" },
				new Category { Id = 80, Name = "MedicationPurpose", Value = "Autoimmune" },
				new Category { Id = 81, Name = "MedicationPurpose", Value = "Digestive" },
				new Category { Id = 82, Name = "MedicationPurpose", Value = "Birth Control" },
				new Category { Id = 83, Name = "MedicationPurpose", Value = "Sex Hormone" },
				new Category { Id = 84, Name = "MedicationPurpose", Value = "Supplement" },
				new Category { Id = 85, Name = "MedicationPurpose", Value = "Other" },

				// MEDICATION USAGE
				new Category { Id = 86, Name = "MedicationUsage", Value = "daily", Description = "Daily" },
				new Category { Id = 87, Name = "MedicationUsage", Value = "bid", Description = "Twice Daily" },
				new Category { Id = 88, Name = "MedicationUsage", Value = "tid", Description = "Three Times Daily" },
				new Category { Id = 89, Name = "MedicationUsage", Value = "q4", Description = "Every 4 Hours" },
				new Category { Id = 90, Name = "MedicationUsage", Value = "q6", Description = "Every 6 Hours" },
				new Category { Id = 91, Name = "MedicationUsage", Value = "prn", Description = "As Needed" },
				new Category { Id = 92, Name = "MedicationUsage", Value = "Other" },

				// MEDICAL CONDITIONS
				new Category { Id = 101, Name = "MedicalCondition", Value = "Anxiety", SubValue = "Phobia"},
				new Category { Id = 102, Name = "MedicalCondition", Value = "Anxiety", SubValue = "Panic disorder"},
				new Category { Id = 103, Name = "MedicalCondition", Value = "Anxiety", SubValue = "Social Anxiety"},
				new Category { Id = 104, Name = "MedicalCondition", Value = "Anxiety", SubValue = "OCD"},
				new Category { Id = 105, Name = "MedicalCondition", Value = "Anxiety", SubValue = "GAD"},
				new Category { Id = 106, Name = "MedicalCondition", Value = "Arthritis", SubValue = "Gout"},
				new Category { Id = 107, Name = "MedicalCondition", Value = "Arthritis", SubValue = "OA"},
				new Category { Id = 108, Name = "MedicalCondition", Value = "Arthritis", SubValue = "Rheumatoid"},
				new Category { Id = 109, Name = "MedicalCondition", Value = "Arthritis", SubValue = "JRA"},
				new Category { Id = 110, Name = "MedicalCondition", Value = "Autism", SubValue = "Asperger"},
				new Category { Id = 111, Name = "MedicalCondition", Value = "Autism", SubValue = "Classic"},
				new Category { Id = 112, Name = "MedicalCondition", Value = "Autism", SubValue = "PDD-NOS"},
				new Category { Id = 113, Name = "MedicalCondition", Value = "Autism", SubValue = "Childhood Disintegrative Disorder"},
				new Category { Id = 114, Name = "MedicalCondition", Value = "Blood Disorder", SubValue = "Hyperkalemia"},
				new Category { Id = 115, Name = "MedicalCondition", Value = "Blood Disorder", SubValue = "Anemia"},
				new Category { Id = 116, Name = "MedicalCondition", Value = "Blood Pressure", SubValue = "Hyper"},
				new Category { Id = 117, Name = "MedicalCondition", Value = "Blood Pressure", SubValue = "Hypo"},
				new Category { Id = 118, Name = "MedicalCondition", Value = "Blood Pressure", SubValue = "Orthostatic Hypo"},
				new Category { Id = 119, Name = "MedicalCondition", Value = "Blood Pressure", SubValue = "Glaucoma"},
				new Category { Id = 120, Name = "MedicalCondition", Value = "Bone", SubValue = "Osteomyelitis"},
				new Category { Id = 121, Name = "MedicalCondition", Value = "Bone", SubValue = "Osteoporosis"},
				new Category { Id = 122, Name = "MedicalCondition", Value = "Bone", SubValue = "Rickets"},
				new Category { Id = 123, Name = "MedicalCondition", Value = "Bone", SubValue = "Osteomalacia"},
				new Category { Id = 124, Name = "MedicalCondition", Value = "Bone", SubValue = "Scoliosis"},
				new Category { Id = 125, Name = "MedicalCondition", Value = "Cancer", SubValue = "Bladder"},
				new Category { Id = 126, Name = "MedicalCondition", Value = "Cancer", SubValue = "Bone"},
				new Category { Id = 127, Name = "MedicalCondition", Value = "Cancer", SubValue = "Brain"},
				new Category { Id = 128, Name = "MedicalCondition", Value = "Cancer", SubValue = "Breats"},
				new Category { Id = 129, Name = "MedicalCondition", Value = "Cancer", SubValue = "Cervical"},
				new Category { Id = 130, Name = "MedicalCondition", Value = "Cancer", SubValue = "Colon"},
				new Category { Id = 131, Name = "MedicalCondition", Value = "Cancer", SubValue = "Leukemia"},
				new Category { Id = 132, Name = "MedicalCondition", Value = "Cancer", SubValue = "Lung"},
				new Category { Id = 133, Name = "MedicalCondition", Value = "Cancer", SubValue = "Liver"},
				new Category { Id = 134, Name = "MedicalCondition", Value = "Cancer", SubValue = "Mesothelioma"},
				new Category { Id = 135, Name = "MedicalCondition", Value = "Cancer", SubValue = "Ovarian"},
				new Category { Id = 136, Name = "MedicalCondition", Value = "Cancer", SubValue = "Prostate"},
				new Category { Id = 137, Name = "MedicalCondition", Value = "Cancer", SubValue = "Skin"},
				new Category { Id = 138, Name = "MedicalCondition", Value = "Cancer", SubValue = "Stomach"},
				new Category { Id = 139, Name = "MedicalCondition", Value = "Cancer", SubValue = "Testicular"},
				new Category { Id = 140, Name = "MedicalCondition", Value = "Cancer", SubValue = "Spine"},
				new Category { Id = 141, Name = "MedicalCondition", Value = "Cancer", SubValue = "Bile Duct"},
				new Category { Id = 142, Name = "MedicalCondition", Value = "Cancer", SubValue = "Pancreatic"},
				new Category { Id = 143, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Angina"},
				new Category { Id = 144, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "MI"},
				new Category { Id = 145, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "CHF-R/L"},
				new Category { Id = 146, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "DVT"},
				new Category { Id = 147, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Cardiomyopathy"},
				new Category { Id = 148, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Stent"},
				new Category { Id = 149, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "CAD"},
				new Category { Id = 150, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "PAD"},
				new Category { Id = 151, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Anuerism"},
				new Category { Id = 152, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Stroke - ischemic/hemorrhagic"},
				new Category { Id = 153, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "TIA"},
				new Category { Id = 154, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Dysrhythmia - SVT/A-Fib"},
				new Category { Id = 155, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Congenital"},
				new Category { Id = 156, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Rheumatic"},
				new Category { Id = 157, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Valvular"},
				new Category { Id = 158, Name = "MedicalCondition", Value = "Cardiovascular", SubValue = "Vasculitis"},
				new Category { Id = 159, Name = "MedicalCondition", Value = "Cholesterol", SubValue = "Hyper"},
				new Category { Id = 160, Name = "MedicalCondition", Value = "Cholesterol", SubValue = "Hypo"},
				new Category { Id = 161, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Fibromyalgia"},
				new Category { Id = 162, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Sciatica"},
				new Category { Id = 163, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Back"},
				new Category { Id = 164, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Headaches"},
				new Category { Id = 165, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Joint - Bursitis/Tendinitis"},
				new Category { Id = 166, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Carpal tunnel"},
				new Category { Id = 167, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Neuralgia"},
				new Category { Id = 168, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Neuropathy"},
				new Category { Id = 169, Name = "MedicalCondition", Value = "Chronic Pain", SubValue = "Migraine"},
				new Category { Id = 170, Name = "MedicalCondition", Value = "Depression", SubValue = "Major"},
				new Category { Id = 171, Name = "MedicalCondition", Value = "Depression", SubValue = "Persistent Depressive Disorder"},
				new Category { Id = 172, Name = "MedicalCondition", Value = "Depression", SubValue = "Bipolar"},
				new Category { Id = 173, Name = "MedicalCondition", Value = "Depression", SubValue = "Seasonal Affective Disorder [SAD]"},
				new Category { Id = 174, Name = "MedicalCondition", Value = "Depression", SubValue = "Psychotic"},
				new Category { Id = 175, Name = "MedicalCondition", Value = "Depression", SubValue = "Peripartum"},
				new Category { Id = 176, Name = "MedicalCondition", Value = "Depression", SubValue = "Postpartum"},
				new Category { Id = 177, Name = "MedicalCondition", Value = "Depression", SubValue = "Premenstrual Dysphoric Disorder [PMDD]"},
				new Category { Id = 178, Name = "MedicalCondition", Value = "Depression", SubValue = "Situational"},
				new Category { Id = 179, Name = "MedicalCondition", Value = "Depression", SubValue = "Atypical"},
				new Category { Id = 180, Name = "MedicalCondition", Value = "Depression", SubValue = "Cyclothymic Disorder"},
				new Category { Id = 181, Name = "MedicalCondition", Value = "Diabetes", SubValue = "Hyperglycemia"},
				new Category { Id = 182, Name = "MedicalCondition", Value = "Diabetes", SubValue = "Juvenile"},
				new Category { Id = 183, Name = "MedicalCondition", Value = "Diabetes", SubValue = "Type 1"},
				new Category { Id = 184, Name = "MedicalCondition", Value = "Diabetes", SubValue = "Type 2"},
				new Category { Id = 185, Name = "MedicalCondition", Value = "Diabetes", SubValue = "Gestational"},
				new Category { Id = 186, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Hypothyroid"},
				new Category { Id = 187, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Hyperthyroid"},
				new Category { Id = 188, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Graves"},
				new Category { Id = 189, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Addison's"},
				new Category { Id = 190, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Cushing's"},
				new Category { Id = 191, Name = "MedicalCondition", Value = "Endocrine", SubValue = "PCOS"},
				new Category { Id = 192, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Hypopituitarism"},
				new Category { Id = 193, Name = "MedicalCondition", Value = "Endocrine", SubValue = "Gigantism"},
				new Category { Id = 194, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "GERD"},
				new Category { Id = 195, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Gallstones"},
				new Category { Id = 196, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Celiac"},
				new Category { Id = 197, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Ulcerative Colitis"},
				new Category { Id = 198, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Crohn's"},
				new Category { Id = 199, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Hemorrhoids"},
				new Category { Id = 200, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Diverticulitis"},
				new Category { Id = 201, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "IBS"},
				new Category { Id = 202, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Ulcer"},
				new Category { Id = 203, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Anal Fissure"},
				new Category { Id = 204, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Hernia"},
				new Category { Id = 205, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Pancreatitis"},
				new Category { Id = 206, Name = "MedicalCondition", Value = "Gastrointestinal", SubValue = "Polyps"},
				new Category { Id = 207, Name = "MedicalCondition", Value = "Genetic", SubValue = "Downs"},
				new Category { Id = 208, Name = "MedicalCondition", Value = "Genetic", SubValue = "Edwards"},
				new Category { Id = 209, Name = "MedicalCondition", Value = "Genetic", SubValue = "Rett Syndrome"},
				new Category { Id = 210, Name = "MedicalCondition", Value = "Genetic", SubValue = "CF"},
				new Category { Id = 211, Name = "MedicalCondition", Value = "Genetic", SubValue = "Sickle Cell Anemia"},
				new Category { Id = 212, Name = "MedicalCondition", Value = "Genetic", SubValue = "Marfan Syndrome"},
				new Category { Id = 213, Name = "MedicalCondition", Value = "Genetic", SubValue = "Huntington's"},
				new Category { Id = 214, Name = "MedicalCondition", Value = "Genetic", SubValue = "Hemochromatosis"},
				new Category { Id = 215, Name = "MedicalCondition", Value = "Genetic", SubValue = "Turner Syndrome"},
				new Category { Id = 216, Name = "MedicalCondition", Value = "Genetic", SubValue = "Obesity"},
				new Category { Id = 217, Name = "MedicalCondition", Value = "Infectious", SubValue = "HIV/AIDS"},
				new Category { Id = 218, Name = "MedicalCondition", Value = "Infectious", SubValue = "Hepatitis - A/B/C/D/E"},
				new Category { Id = 219, Name = "MedicalCondition", Value = "Infectious", SubValue = "Herpes"},
				new Category { Id = 220, Name = "MedicalCondition", Value = "Infectious", SubValue = "Malaria"},
				new Category { Id = 221, Name = "MedicalCondition", Value = "Infectious", SubValue = "Polio"},
				new Category { Id = 222, Name = "MedicalCondition", Value = "Infectious", SubValue = "Scabies"},
				new Category { Id = 223, Name = "MedicalCondition", Value = "Infectious", SubValue = "MRSA"},
				new Category { Id = 224, Name = "MedicalCondition", Value = "Infectious", SubValue = "TB"},
				new Category { Id = 225, Name = "MedicalCondition", Value = "Infectious", SubValue = "Dengue"},
				new Category { Id = 226, Name = "MedicalCondition", Value = "Infectious", SubValue = "TD"},
				new Category { Id = 227, Name = "MedicalCondition", Value = "Infectious", SubValue = "Ebola"},
				new Category { Id = 228, Name = "MedicalCondition", Value = "Neurology", SubValue = "ADHD"},
				new Category { Id = 229, Name = "MedicalCondition", Value = "Neurology", SubValue = "ADD"},
				new Category { Id = 230, Name = "MedicalCondition", Value = "Neurology", SubValue = "Alzheimer's"},
				new Category { Id = 231, Name = "MedicalCondition", Value = "Neurology", SubValue = "Dementia"},
				new Category { Id = 232, Name = "MedicalCondition", Value = "Neurology", SubValue = "ALS"},
				new Category { Id = 233, Name = "MedicalCondition", Value = "Neurology", SubValue = "Epilepsy"},
				new Category { Id = 234, Name = "MedicalCondition", Value = "Neurology", SubValue = "MS"},
				new Category { Id = 235, Name = "MedicalCondition", Value = "Neurology", SubValue = "PD"},
				new Category { Id = 236, Name = "MedicalCondition", Value = "Neurology", SubValue = "RLS"},
				new Category { Id = 237, Name = "MedicalCondition", Value = "Neurology", SubValue = "Neurodegeneration"},
				new Category { Id = 238, Name = "MedicalCondition", Value = "Neurology", SubValue = "CP"},
				new Category { Id = 239, Name = "MedicalCondition", Value = "Neurology", SubValue = "MD"},
				new Category { Id = 240, Name = "MedicalCondition", Value = "Neurology", SubValue = "Neuralgia"},
				new Category { Id = 241, Name = "MedicalCondition", Value = "Neurology", SubValue = "Neuropathy"},
				new Category { Id = 242, Name = "MedicalCondition", Value = "Neurology", SubValue = "Seizures"},
				new Category { Id = 243, Name = "MedicalCondition", Value = "Neurology", SubValue = "Spinal Cord Injury"},
				new Category { Id = 244, Name = "MedicalCondition", Value = "Neurology", SubValue = "Vertigo"},
				new Category { Id = 245, Name = "MedicalCondition", Value = "Organs", SubValue = "Nephropathy"},
				new Category { Id = 246, Name = "MedicalCondition", Value = "Organs", SubValue = "Renal Failure"},
				new Category { Id = 247, Name = "MedicalCondition", Value = "Organs", SubValue = "Liver"},
				new Category { Id = 248, Name = "MedicalCondition", Value = "Other Autoimmune", SubValue = "Lupus"},
				new Category { Id = 249, Name = "MedicalCondition", Value = "Other Autoimmune", SubValue = "Sjogren's syndrome"},
				new Category { Id = 250, Name = "MedicalCondition", Value = "Other Autoimmune", SubValue = "Myasthenia Gravis"},
				new Category { Id = 251, Name = "MedicalCondition", Value = "Other Autoimmune", SubValue = "Hashimoto's Thyroiditis"},
				new Category { Id = 252, Name = "MedicalCondition", Value = "Prosthesis", SubValue = "Leg"},
				new Category { Id = 253, Name = "MedicalCondition", Value = "Prosthesis", SubValue = "Arm"},
				new Category { Id = 254, Name = "MedicalCondition", Value = "Prosthesis", SubValue = "Eye"},
				new Category { Id = 255, Name = "MedicalCondition", Value = "Prosthesis", SubValue = "Breast"},
				new Category { Id = 256, Name = "MedicalCondition", Value = "Psychological", SubValue = "Borderline"},
				new Category { Id = 257, Name = "MedicalCondition", Value = "Psychological", SubValue = "Dependency"},
				new Category { Id = 258, Name = "MedicalCondition", Value = "Psychological", SubValue = "Dissociative Identity"},
				new Category { Id = 259, Name = "MedicalCondition", Value = "Psychological", SubValue = "Narcissistic"},
				new Category { Id = 260, Name = "MedicalCondition", Value = "Psychological", SubValue = "Paranoid"},
				new Category { Id = 261, Name = "MedicalCondition", Value = "Psychological", SubValue = "Phobia"},
				new Category { Id = 262, Name = "MedicalCondition", Value = "Psychological", SubValue = "PTSD"},
				new Category { Id = 263, Name = "MedicalCondition", Value = "Psychological", SubValue = "RAD"},
				new Category { Id = 264, Name = "MedicalCondition", Value = "Psychological", SubValue = "Schizophrenia"},
				new Category { Id = 265, Name = "MedicalCondition", Value = "Psychological", SubValue = "Anorexia Nervosa"},
				new Category { Id = 266, Name = "MedicalCondition", Value = "Psychological", SubValue = "Bulimia"},
				new Category { Id = 267, Name = "MedicalCondition", Value = "Psychological", SubValue = "Binge Eating"},
				new Category { Id = 268, Name = "MedicalCondition", Value = "Psychological", SubValue = "Impulse Control"},
				new Category { Id = 269, Name = "MedicalCondition", Value = "Psychological", SubValue = "Personality"},
				new Category { Id = 270, Name = "MedicalCondition", Value = "Psychological", SubValue = "Stress Response Syndrome"},
				new Category { Id = 271, Name = "MedicalCondition", Value = "Psychological", SubValue = "Somatic Symptom Disorder"},
				new Category { Id = 272, Name = "MedicalCondition", Value = "Psychological", SubValue = "Tic Disorder [Tourette's]"},
				new Category { Id = 273, Name = "MedicalCondition", Value = "Respiratory", SubValue = "Asthma"},
				new Category { Id = 274, Name = "MedicalCondition", Value = "Respiratory", SubValue = "COPD"},
				new Category { Id = 275, Name = "MedicalCondition", Value = "Respiratory", SubValue = "Emphysema"},
				new Category { Id = 276, Name = "MedicalCondition", Value = "Respiratory", SubValue = "SARS"},
				new Category { Id = 277, Name = "MedicalCondition", Value = "Respiratory", SubValue = "CF"},
				new Category { Id = 278, Name = "MedicalCondition", Value = "Respiratory", SubValue = "Sleep Apnea"},
				new Category { Id = 279, Name = "MedicalCondition", Value = "Sexual", SubValue = "Enlarged Prostate"},
				new Category { Id = 280, Name = "MedicalCondition", Value = "Sexual", SubValue = "Erectile Dysfunction"},
				new Category { Id = 281, Name = "MedicalCondition", Value = "Sexual", SubValue = "Genital Wart"},
				new Category { Id = 282, Name = "MedicalCondition", Value = "Sexual", SubValue = "STD"},
				new Category { Id = 283, Name = "MedicalCondition", Value = "Sexual", SubValue = "Infertility"},
				new Category { Id = 284, Name = "MedicalCondition", Value = "Sexual", SubValue = "PMS"},
				new Category { Id = 285, Name = "MedicalCondition", Value = "Skin", SubValue = "Dermatitis"},
				new Category { Id = 286, Name = "MedicalCondition", Value = "Skin", SubValue = "Psoriasis"},
				new Category { Id = 287, Name = "MedicalCondition", Value = "Skin", SubValue = "Eczema"},
				new Category { Id = 288, Name = "MedicalCondition", Value = "Skin", SubValue = "Rosacea"},
				new Category { Id = 289, Name = "MedicalCondition", Value = "Skin", SubValue = "Lupus"},
				new Category { Id = 290, Name = "MedicalCondition", Value = "Other"},

				// ALLERGY TYPES
				new Category { Id = 291, Name = "Allergy", Value = "Drug", SubValue = "Antibiotics" },
				new Category { Id = 292, Name = "Allergy", Value = "Drug", SubValue = "Nonsteroidal anti-inflammatory" },
				new Category { Id = 293, Name = "Allergy", Value = "Drug", SubValue = "Aspirin" },
				new Category { Id = 294, Name = "Allergy", Value = "Drug", SubValue = "Sulfa drugs" },
				new Category { Id = 295, Name = "Allergy", Value = "Drug", SubValue = "Chemotherapy drugs" },
				new Category { Id = 296, Name = "Allergy", Value = "Drug", SubValue = "Monoclonal antibody therapy" },
				new Category { Id = 297, Name = "Allergy", Value = "Drug", SubValue = "HIV drugs" },
				new Category { Id = 298, Name = "Allergy", Value = "Drug", SubValue = "Insulin" },
				new Category { Id = 299, Name = "Allergy", Value = "Drug", SubValue = "Antiseizure" },
				new Category { Id = 300, Name = "Allergy", Value = "Drug", SubValue = "Muscle relaxers" },
				new Category { Id = 301, Name = "Allergy", Value = "Drug", SubValue = "Other" },
				new Category { Id = 302, Name = "Allergy", Value = "Food", SubValue = "Milk" },
				new Category { Id = 303, Name = "Allergy", Value = "Food", SubValue = "Egg" },
				new Category { Id = 304, Name = "Allergy", Value = "Food", SubValue = "Peanut" },
				new Category { Id = 305, Name = "Allergy", Value = "Food", SubValue = "Tree Nut" },
				new Category { Id = 306, Name = "Allergy", Value = "Food", SubValue = "Soy" },
				new Category { Id = 307, Name = "Allergy", Value = "Food", SubValue = "Wheat" },
				new Category { Id = 308, Name = "Allergy", Value = "Food", SubValue = "Fish" },
				new Category { Id = 309, Name = "Allergy", Value = "Food", SubValue = "Insect" },
				new Category { Id = 310, Name = "Allergy", Value = "Food", SubValue = "Shellfish (crustaceans)" },
				new Category { Id = 311, Name = "Allergy", Value = "Food", SubValue = "Other" },
				new Category { Id = 312, Name = "Allergy", Value = "Insect", SubValue = "Mosquito Bites" },
				new Category { Id = 313, Name = "Allergy", Value = "Insect", SubValue = "Scorpion Stings" },
				new Category { Id = 314, Name = "Allergy", Value = "Insect", SubValue = "Bug Spray" },
				new Category { Id = 315, Name = "Allergy", Value = "Insect", SubValue = "Cockroaches" },
				new Category { Id = 316, Name = "Allergy", Value = "Insect", SubValue = "Bee Stings" },
				new Category { Id = 317, Name = "Allergy", Value = "Insect", SubValue = "Fire Ants" },
				new Category { Id = 318, Name = "Allergy", Value = "Insect", SubValue = "Dust Mites" },
				new Category { Id = 319, Name = "Allergy", Value = "Insect", SubValue = "Other" },
				new Category { Id = 320, Name = "Allergy", Value = "Latex"},
				new Category { Id = 321, Name = "Allergy", Value = "Mold", SubValue = "Alternaria" },
				new Category { Id = 322, Name = "Allergy", Value = "Mold", SubValue = "Cladosporium" },
				new Category { Id = 323, Name = "Allergy", Value = "Mold", SubValue = "Aspergillus" },
				new Category { Id = 324, Name = "Allergy", Value = "Mold", SubValue = "Penicillium" },
				new Category { Id = 325, Name = "Allergy", Value = "Mold", SubValue = "Helminthosporum" },
				new Category { Id = 326, Name = "Allergy", Value = "Mold", SubValue = "Epicoccum" },
				new Category { Id = 327, Name = "Allergy", Value = "Mold", SubValue = "Fusarium" },
				new Category { Id = 328, Name = "Allergy", Value = "Mold", SubValue = "Aureobasidium" },
				new Category { Id = 329, Name = "Allergy", Value = "Mold", SubValue = "Phoma" },
				new Category { Id = 330, Name = "Allergy", Value = "Mold", SubValue = "Smuts" },
				new Category { Id = 331, Name = "Allergy", Value = "Mold", SubValue = "Rhizopus and Mucor" },
				new Category { Id = 332, Name = "Allergy", Value = "Mold", SubValue = "Yeasts" },
				new Category { Id = 333, Name = "Allergy", Value = "Mold", SubValue = "Other" },
				new Category { Id = 334, Name = "Allergy", Value = "Pet", SubValue = "Cat" },
				new Category { Id = 335, Name = "Allergy", Value = "Pet", SubValue = "Dog" },
				new Category { Id = 336, Name = "Allergy", Value = "Pet", SubValue = "Rodent" },
				new Category { Id = 337, Name = "Allergy", Value = "Pet", SubValue = "Other" },
				new Category { Id = 338, Name = "Allergy", Value = "Pollen"},
				new Category { Id = 339, Name = "Allergy", Value = "Other"},

				// SEVERITY
				new Category { Id = 340, Name = "Severity", Value = "Mild" },
				new Category { Id = 341, Name = "Severity", Value = "Moderate" },
				new Category { Id = 342, Name = "Severity", Value = "Severe" },
				new Category { Id = 343, Name = "Severity", Value = "Critical" },

				// INJURY TYPES
				new Category { Id = 501, Name = "Injury", Value = "Muscular", SubValue = "Strain" },
				new Category { Id = 502, Name = "Injury", Value = "Muscular", SubValue = "Sprain" },
				new Category { Id = 503, Name = "Injury", Value = "Muscular", SubValue = "Other" },
				new Category { Id = 504, Name = "Injury", Value = "Fracture", SubValue = "Clavicle" },
				new Category { Id = 505, Name = "Injury", Value = "Fracture", SubValue = "Ankle" },
				new Category { Id = 506, Name = "Injury", Value = "Fracture", SubValue = "Wrist" },
				new Category { Id = 507, Name = "Injury", Value = "Fracture", SubValue = "Hip" },
				new Category { Id = 508, Name = "Injury", Value = "Fracture", SubValue = "Radius/Ulna" },
				new Category { Id = 509, Name = "Injury", Value = "Fracture", SubValue = "Femur" },
				new Category { Id = 510, Name = "Injury", Value = "Fracture", SubValue = "Spinal" },
				new Category { Id = 511, Name = "Injury", Value = "Fracture", SubValue = "Cervical" },
				new Category { Id = 512, Name = "Injury", Value = "Fracture", SubValue = "Skull" },
				new Category { Id = 513, Name = "Injury", Value = "Fracture", SubValue = "Other" },
				new Category { Id = 514, Name = "Injury", Value = "Brain", SubValue = "Concussion" },
				new Category { Id = 515, Name = "Injury", Value = "Brain", SubValue = "Hemorrhage" },
				new Category { Id = 516, Name = "Injury", Value = "Brain", SubValue = "Edema" },
				new Category { Id = 517, Name = "Injury", Value = "Brain", SubValue = "Fracture" },
				new Category { Id = 518, Name = "Injury", Value = "Brain", SubValue = "Diffuse Axonal" },
				new Category { Id = 519, Name = "Injury", Value = "Brain", SubValue = "Other" },
				new Category { Id = 520, Name = "Injury", Value = "Knee", SubValue = "Fracture" },
				new Category { Id = 521, Name = "Injury", Value = "Knee", SubValue = "ACL" },
				new Category { Id = 522, Name = "Injury", Value = "Knee", SubValue = "Dislocation" },
				new Category { Id = 523, Name = "Injury", Value = "Knee", SubValue = "Meniscal" },
				new Category { Id = 524, Name = "Injury", Value = "Knee", SubValue = "Ligament Tear" },
				new Category { Id = 525, Name = "Injury", Value = "Knee", SubValue = "Other" },
				new Category { Id = 526, Name = "Injury", Value = "Spinal", SubValue = "Sprain" },
				new Category { Id = 527, Name = "Injury", Value = "Spinal", SubValue = "Strain" },
				new Category { Id = 528, Name = "Injury", Value = "Spinal", SubValue = "Nerve Damage" },
				new Category { Id = 529, Name = "Injury", Value = "Spinal", SubValue = "Fracture" },
				new Category { Id = 530, Name = "Injury", Value = "Spinal", SubValue = "Dislocation" },
				new Category { Id = 531, Name = "Injury", Value = "Spinal", SubValue = "Herniated Disc" },
				new Category { Id = 532, Name = "Injury", Value = "Spinal", SubValue = "Other" },
				new Category { Id = 533, Name = "Injury", Value = "Burn", SubValue = "Thermal" },
				new Category { Id = 534, Name = "Injury", Value = "Burn", SubValue = "Electrical" },
				new Category { Id = 535, Name = "Injury", Value = "Burn", SubValue = "Chemical" },
				new Category { Id = 536, Name = "Injury", Value = "Burn", SubValue = "Friction" },
				new Category { Id = 537, Name = "Injury", Value = "Burn", SubValue = "Radiation" },
				new Category { Id = 538, Name = "Injury", Value = "Burn", SubValue = "Other" },
				new Category { Id = 539, Name = "Injury", Value = "Amputation"},
				new Category { Id = 540, Name = "Injury", Value = "Crush"},
				new Category { Id = 541, Name = "Injury", Value = "Acoustic"},
				new Category { Id = 542, Name = "Injury", Value = "TBI"},
				new Category { Id = 543, Name = "Injury", Value = "Stab"},
				new Category { Id = 544, Name = "Injury", Value = "GSW"},
				new Category { Id = 545, Name = "Injury", Value = "Electrocution"},
				new Category { Id = 546, Name = "Injury", Value = "Drowning"},
				new Category { Id = 547, Name = "Injury", Value = "Other"},

				// SURGERY TYPES
				new Category { Id = 601, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "CABG" },
				new Category { Id = 602, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "TMR" },
				new Category { Id = 603, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Heart Valve Repair" },
				new Category { Id = 604, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Heart Valve Replacement" },
				new Category { Id = 605, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Transplant" },
				new Category { Id = 606, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "ICD" },
				new Category { Id = 607, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Aneurysm" },
				new Category { Id = 608, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Pacemaker" },
				new Category { Id = 609, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Angioplasty" },
				new Category { Id = 610, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Atherectomy" },
				new Category { Id = 611, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Cardiomyoplasty" },
				new Category { Id = 612, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Bypass" },
				new Category { Id = 613, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Stent" },
				new Category { Id = 614, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Ablation" },
				new Category { Id = 615, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Artificial Heart" },
				new Category { Id = 616, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "LVAD" },
				new Category { Id = 617, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Continuous-Flow Heart Replacement" },
				new Category { Id = 618, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Cardotid Endarterectomy" },
				new Category { Id = 619, Name = "Surgery", Value = "Cardio-Vascular", SubValue = "Other" },
				new Category { Id = 620, Name = "Surgery", Value = "Pregnancy", SubValue = "Vaginal" },
				new Category { Id = 621, Name = "Surgery", Value = "Pregnancy", SubValue = "Cesarean Section" },
				new Category { Id = 622, Name = "Surgery", Value = "Pregnancy", SubValue = "Miscarriage" },
				new Category { Id = 623, Name = "Surgery", Value = "Pregnancy", SubValue = "Preeclampsia" },
				new Category { Id = 624, Name = "Surgery", Value = "Pregnancy", SubValue = "Ectopic" },
				new Category { Id = 625, Name = "Surgery", Value = "Pregnancy", SubValue = "Placenta Previa" },
				new Category { Id = 626, Name = "Surgery", Value = "Pregnancy", SubValue = "Oligohydramnios" },
				new Category { Id = 627, Name = "Surgery", Value = "Pregnancy", SubValue = "Gestational Diabetes" },
				new Category { Id = 628, Name = "Surgery", Value = "Pregnancy", SubValue = "Other" },
				new Category { Id = 629, Name = "Surgery", Value = "Would Debridement" },
				new Category { Id = 630, Name = "Surgery", Value = "Gynecological", SubValue = "D&C" },
				new Category { Id = 631, Name = "Surgery", Value = "Gynecological", SubValue = "Tubal Ligation" },
				new Category { Id = 632, Name = "Surgery", Value = "Gynecological", SubValue = "UAE" },
				new Category { Id = 633, Name = "Surgery", Value = "Gynecological", SubValue = "Endometrial Ablation" },
				new Category { Id = 634, Name = "Surgery", Value = "Gynecological", SubValue = "Other" },
				new Category { Id = 635, Name = "Surgery", Value = "Gastro-Intestinal", SubValue = "Gallstones" },
				new Category { Id = 636, Name = "Surgery", Value = "Gastro-Intestinal", SubValue = "Gastric Bypass" },
				new Category { Id = 637, Name = "Surgery", Value = "Gastro-Intestinal", SubValue = "Other" },
				new Category { Id = 638, Name = "Surgery", Value = "Skin Graft" },
				new Category { Id = 639, Name = "Surgery", Value = "Organ Removal", SubValue = "Hemorrhoidectomy" },
				new Category { Id = 640, Name = "Surgery", Value = "Organ Removal", SubValue = "Hysterectomy" },
				new Category { Id = 641, Name = "Surgery", Value = "Organ Removal", SubValue = "Mastectomy" },
				new Category { Id = 642, Name = "Surgery", Value = "Organ Removal", SubValue = "Colectomy" },
				new Category { Id = 643, Name = "Surgery", Value = "Organ Removal", SubValue = "Prostatectomy" },
				new Category { Id = 644, Name = "Surgery", Value = "Organ Removal", SubValue = "Tonsillectomy" },
				new Category { Id = 645, Name = "Surgery", Value = "Organ Removal", SubValue = "Cholecystectomy" },
				new Category { Id = 646, Name = "Surgery", Value = "Organ Removal", SubValue = "Appendectomy" },
				new Category { Id = 647, Name = "Surgery", Value = "Organ Removal", SubValue = "Other" },
				new Category { Id = 648, Name = "Surgery", Value = "Eye", SubValue = "Cataract" },
				new Category { Id = 649, Name = "Surgery", Value = "Eye", SubValue = "Laser" },
				new Category { Id = 650, Name = "Surgery", Value = "Eye", SubValue = "Other" },
				new Category { Id = 651, Name = "Surgery", Value = "Reconstructive" },
				new Category { Id = 652, Name = "Surgery", Value = "Urology", SubValue = "Prostate Cancer" },
				new Category { Id = 653, Name = "Surgery", Value = "Urology", SubValue = "Testicular Cancer" },
				new Category { Id = 654, Name = "Surgery", Value = "Urology", SubValue = "Kidney Cancer" },
				new Category { Id = 655, Name = "Surgery", Value = "Urology", SubValue = "Bladder Cancer" },
				new Category { Id = 656, Name = "Surgery", Value = "Urology", SubValue = "Kidney Stones" },
				new Category { Id = 657, Name = "Surgery", Value = "Urology", SubValue = "Bladder Stones" },
				new Category { Id = 658, Name = "Surgery", Value = "Urology", SubValue = "Other" },
				new Category { Id = 659, Name = "Surgery", Value = "Neuro", SubValue = "Brain Tumor" },
				new Category { Id = 660, Name = "Surgery", Value = "Neuro", SubValue = "Cerebral Aneurysm" },
				new Category { Id = 661, Name = "Surgery", Value = "Neuro", SubValue = "Microdiscectomies" },
				new Category { Id = 662, Name = "Surgery", Value = "Neuro", SubValue = "Other" },
				new Category { Id = 663, Name = "Surgery", Value = "Orthopedic", SubValue = "Achilles Tear" },
				new Category { Id = 664, Name = "Surgery", Value = "Orthopedic", SubValue = "Meniscus Repair" },
				new Category { Id = 665, Name = "Surgery", Value = "Orthopedic", SubValue = "Ankle Replacement" },
				new Category { Id = 666, Name = "Surgery", Value = "Orthopedic", SubValue = "Knee Surgery" },
				new Category { Id = 667, Name = "Surgery", Value = "Orthopedic", SubValue = "Knee Replacement" },
				new Category { Id = 668, Name = "Surgery", Value = "Orthopedic", SubValue = "Shoulder Surgery" },
				new Category { Id = 669, Name = "Surgery", Value = "Orthopedic", SubValue = "Shoulder Replacement" },
				new Category { Id = 670, Name = "Surgery", Value = "Orthopedic", SubValue = "Spinal Surgery" },
				new Category { Id = 671, Name = "Surgery", Value = "Orthopedic", SubValue = "Spinal Fusion" },
				new Category { Id = 672, Name = "Surgery", Value = "Orthopedic", SubValue = "Hip Surgery" },
				new Category { Id = 673, Name = "Surgery", Value = "Orthopedic", SubValue = "Hip Replacement" },
				new Category { Id = 674, Name = "Surgery", Value = "Orthopedic", SubValue = "Other" },
				new Category { Id = 675, Name = "Surgery", Value = "Other" }
			};
			modelBuilder.Entity<Category>().HasData(
				types
				);
		}

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException("Value cannot be empty or whitespace only string.", "password");

            using var hmac = new HMACSHA512();
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        }
    }
}
