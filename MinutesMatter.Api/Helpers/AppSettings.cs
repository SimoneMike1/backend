﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinutesMatter.Api.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
