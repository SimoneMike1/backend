﻿using AutoMapper;
using MinutesMatter.Api.Entities;
using MinutesMatter.Api.Models.Accounts;

namespace MinutesMatter.Api.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Account, AccountModel>();
            CreateMap<RegisterModel, Account>();
            CreateMap<UpdateModel, Account>();
        }
    }
}
